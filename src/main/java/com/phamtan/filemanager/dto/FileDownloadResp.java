package com.phamtan.filemanager.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.Resource;

@Getter
@Setter
@Builder
public class FileDownloadResp {

    private String contentType;
    private Resource resource;

}

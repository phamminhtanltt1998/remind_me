package com.phamtan.filemanager.service;

import com.phamtan.filemanager.dto.FileDownloadResp;
import com.phamtan.filemanager.dto.FileResp;
import com.phamtan.filemanager.entity.FileEntity;
import com.phamtan.filemanager.exceptions.StorageException;
import com.phamtan.filemanager.repo.FileRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class FileService {

    private Path rootLocation;
    private final FileRepo fileRepo;

    @PostConstruct
    public void init() {
        this.rootLocation = Paths.get("/home/phamminhtan/work/storage");
    }

    @Transactional(rollbackOn = StorageException.class)
    public FileResp uploadFile(MultipartFile file) throws IOException, StorageException {
        log.info("Start upload file");
        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
        }
        try {

            Path destinationPath = this.rootLocation.resolve(file.getOriginalFilename());
            FileEntity fileEntity = new FileEntity();
            fileEntity.setId(UUID.randomUUID().toString());
            fileEntity.setPath(destinationPath.normalize().toString());
            fileEntity.setOwner("default");
            fileEntity.setName(file.getName());
            fileEntity.setContentType(file.getContentType());
            FileEntity fileSave = fileRepo.save(fileEntity);

            log.info("save file to folder :{}", destinationPath.normalize());
            Files.copy(file.getInputStream(), destinationPath);

            return FileResp.builder()
                    .id(fileSave.getId())
                    .path(fileSave.getPath())
                    .owner(fileSave.getOwner())
                    .name(fileSave.getName())
                    .createdBy(fileSave.getCreatedBy())
                    .createdDate(fileSave.getCreatedDate())
                    .updatedBy(fileSave.getUpdatedBy())
                    .updatedDate(fileSave.getUpdatedDate())
                    .type(fileSave.getContentType())
                    .build();
        } catch (Exception exception) {
            log.info("Upload file error :{}", exception);
            throw new StorageException("Failed to store file " + file.getOriginalFilename());
        }
    }

    public List<FileResp> getAllFileResp() {
        List<FileEntity> files = fileRepo.findAll();
        return files.stream().map(FileResp::mapFromFileInDB).collect(Collectors.toList());
    }

    public FileDownloadResp downloadFile(String id, HttpServletResponse response) throws StorageException, IOException {
        FileEntity file = fileRepo.findById(id)
                .orElseThrow(() -> new StorageException("Not found file : " + id));
        Path path = Path.of(file.getPath());
        Resource resource = new UrlResource(path.toUri());
        if (resource.exists() || resource.isReadable()) {
            return FileDownloadResp.builder()
                    .contentType(file.getContentType())
                    .resource(resource)
                    .build();
        } else {
            throw new StorageException("Could not read the file!");
        }
    }
}

package com.phamtan.filemanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "File")
@Entity(name = "File")
@Getter
@Setter
public class FileEntity {

    @Id
    private String id;
    private String name;
    private String path;
    private String owner;
    private String contentType;
    @CreatedBy
    private String createdBy;
    @CreatedDate
    private String createdDate;
    @LastModifiedBy
    private String updatedBy;
    @LastModifiedDate
    private String updatedDate;

}

package com.phamtan.filemanager.controller;

import com.phamtan.filemanager.dto.BaseResponse;
import com.phamtan.filemanager.dto.FileDownloadResp;
import com.phamtan.filemanager.dto.FileResp;
import com.phamtan.filemanager.service.FileService;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/v1.0/file")
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;

    @GetMapping
    @ApiModelProperty("Get all Files")
    public ResponseEntity<BaseResponse<Object, Object>> getAllFile() {
        return new ResponseEntity<>(new BaseResponse<>(fileService.getAllFileResp()), HttpStatus.OK);
    }

    @PostMapping("/upload")
    public ResponseEntity<BaseResponse<FileResp, Object>> uploadFile(
            @ApiParam("File upload content")
            @RequestParam("file") MultipartFile file) throws IOException {
        return new ResponseEntity<>(new BaseResponse<>(fileService.uploadFile(file)), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> downloadFile(
            @ApiParam("Id of file")
            @PathVariable("id") String id,
            HttpServletResponse response) throws IOException {
        FileDownloadResp fileDownloadResp = fileService.downloadFile(id, response);
        return ResponseEntity.ok()
                .header("Content-Type", fileDownloadResp.getContentType())
                .body(fileDownloadResp.getResource());
    }
}

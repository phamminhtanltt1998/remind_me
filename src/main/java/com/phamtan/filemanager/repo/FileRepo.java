package com.phamtan.filemanager.repo;

import com.phamtan.filemanager.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FileRepo extends JpaRepository<FileEntity, String> {

    Optional<FileEntity> findById(String id);

}

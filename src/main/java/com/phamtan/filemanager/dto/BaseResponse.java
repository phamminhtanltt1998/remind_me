package com.phamtan.filemanager.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.swagger.annotations.ApiParam;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonPropertyOrder({"status", "time"})
public class BaseResponse<T, E> {
    @ApiParam("Time response")

    private String time;
    @ApiParam("Status response")
    private String status;
    @ApiParam("Content")
    private T data;
    @ApiParam("Error ")
    private E error;

    public BaseResponse(T data, E error) {
        this.time = LocalDateTime.now().toString();
        this.status = "OK";
        this.data = data;
        this.error = error;
    }

    public BaseResponse(T data) {
        this.data = data;
        this.time = LocalDateTime.now().toString();
        this.status = "OK";
    }


}

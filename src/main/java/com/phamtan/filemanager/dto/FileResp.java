package com.phamtan.filemanager.dto;

import com.phamtan.filemanager.entity.FileEntity;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FileResp {

    @ApiParam("Id of file")
    private String id;
    @ApiParam("Name of file")
    private String name;
    @ApiParam("Path of file")
    private String path;
    private String owner;
    private String createdBy;
    private String createdDate;
    private String updatedBy;
    private String updatedDate;
    private String type;

    public static FileResp mapFromFileInDB(FileEntity fileEntity) {
        return FileResp.builder()
                .id(fileEntity.getId())
                .name(fileEntity.getName())
                .path(fileEntity.getPath())
                .owner(fileEntity.getOwner())
                .createdBy(fileEntity.getCreatedBy())
                .createdDate(fileEntity.getCreatedDate())
                .updatedDate(fileEntity.getUpdatedDate())
                .updatedBy(fileEntity.getUpdatedBy())
                .type(fileEntity.getContentType())
                .build();
    }

}
